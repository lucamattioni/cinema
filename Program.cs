﻿using System;
using System.Collections.Generic;
using System.IO;
using MySql.Data.MySqlClient;
using CsvHelper;


namespace scritturaDB
{
    class Program
    {
        static void Main(string[] args)
        {
            string cs = @"server=localhost;userid=lucamattioni;password=1396;database=analisi_programmazione;";
            using var con = new MySqlConnection(cs);
            con.Open();
            Console.WriteLine($"mysqlversion:{con.ServerVersion} ");
            //using var cmd = new MySqlCommand();
            /*cmd.Connection=con;
            cmd.CommandText="insert into movie(titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produzione,distributore) values ('lotr', 'IlSignoredegliAnelli', '2001', '1', '178', 'nvz', 'Medusa')";
            cmd.CommandText="insert into movie(titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produzione,distributore) values ('PrinceCaspian', 'Il principe Caspian', '2008', '2', '143', 'usa', 'waltdisney')";
            cmd.CommandText="insert into movie(titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produzione,distributore) values ('Maleficent', 'Maleficent', '2014', '3', '97', 'usa', 'waltdisney')";
            cmd.CommandText="insert into movie(titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produzione,distributore) values ('MistressofEvil'', 'Signoradelmale', '2019', '4', '118', 'usa', 'waltdisney')";
            cmd.ExecuteNonQuery();*/
            //var sql = "insert into movie(titolo_originale, titolo_italiano, anno_produzione, regia_ID, durata, paese_produzione, distributore) values (@titolo_originale, @titolo_italiano, @anno_produzione, @regia_ID, @durata, @paese_produzione, @distributore)";

            /*try
            {
                cmd.ExecuteNonQuery();
                //Console.WriteLine("la riga è stata inserita");
            }
            catch (Exception e) { Console.WriteLine("Errore" + e.ToString()); }*/



            //string[,] matrice = { { "Batman begins", "Batman begins", "2005", "7", "140", "USA", "Warner Bros" }, { "The dark knight", "Il cavaliere oscuro", "2008", "7", "152", "USA", "Warner Bros" }, { "The dark knight rises", "Il cavaliere oscuro il ritorno", "2012", "7", "164", "USA", "Warner Bros" }, { "Night at the museum", "Una notte al museo", "2006", "8", "108", "USA", "20th centuryfox" }, { "Night at the Museum: Battle of the Smithsonian", "Una notte al museo 2 - La fuga", "2009", "8", "105", "USA", "20th centuryfox" } };
            //using var cmd = new MySqlCommand(sql, con);




            /*for (int j = 0; j < 5; j++)

            {
                var sql = "INSERT INTO movie(titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produzione,distributore)VALUES(@titolo_originale,@titolo_italiano,@anno_produzione,@regia_ID,@durata,@paese_produzione,@distributore)";
                using var cmd = new MySqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@titolo_originale", matrice[j, 0]);
                cmd.Parameters.AddWithValue("@titolo_italiano", matrice[j, 1]);
                cmd.Parameters.AddWithValue("@anno_produzione", matrice[j, 2]);
                cmd.Parameters.AddWithValue("@regia_ID", matrice[j, 3]);
                cmd.Parameters.AddWithValue("@durata", matrice[j, 4]);
                cmd.Parameters.AddWithValue("@paese_produzione", matrice[j, 5]);
                cmd.Parameters.AddWithValue("@distributore", matrice[j, 6]);
                cmd.Prepare();
                try
                {
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("La riga è stata inserita");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Errore " + e.ToString());
                }
            }*/


            TextReader reader1 = new StreamReader("attori.csv");
            var csvReader1 = new CsvReader(reader1, System.Globalization.CultureInfo.CurrentCulture);
            var actorsfromcsv = csvReader1.GetRecords<Actor>();

            foreach (Actor actorfromcsv in actorsfromcsv)
            { 
                Console.WriteLine(" " + actorfromcsv.Name + " " + actorfromcsv.Surname);// Console.WriteLine(" " + actorfromcsv.Surname);
            }



               /*for (int i = 0; i < 5; i++)
                {
                var sql2 = "INSERT INTO movie(nome,cognome,anno,cod_fisc,ruolo,note)VALUES(@nome,@cognome,@anno,@cod_fisc,@ruolo,@note)";
                using var cmd2 = new MySqlCommand(sql2, con);
                cmd2.Parameters.AddWithValue("@nome", matrice[i, 0]);
                cmd2.Parameters.AddWithValue("@cognome", matrice[i, 1]);
                cmd2.Parameters.AddWithValue("@anno", matrice[i, 2]);
                cmd2.Parameters.AddWithValue("@cod_fisc", matrice[i, 3]);
                cmd2.Parameters.AddWithValue("@ruolo", matrice[i, 4]);
                cmd2.Prepare();
                try
                {
                    cmd2.ExecuteNonQuery();
                    Console.WriteLine("La riga è stata inserita");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Errore " + e.ToString());
                }
                }*/



                /*try{
                    cmd.ExecuteNonQuery():
                    Console.WriteLine ("la riga è stata aggiunta");
                }
                catch(Exception e){
                }*/

            }
        }

       class Person
    {
        private string _indirizzo;
        //  public string Name { get; set; }
        private string _name;
        public string Name
        {
            get => _name;
            set => _name = value;
        }
        public string Surname { get; set; }
        public int Year { get; set; }
        private string _fiscalcode;  // the name field
        private int _eta;  // the name field
        public string FiscalCode    // the Name property
        {
            get => _fiscalcode;
            set => _fiscalcode = value;
        }
        public string StampaMessaggio(string messaggino)
        {
            //questa è una virtual
            return "questo è il metodo della persona" + messaggino;
        }
        protected int Age()    // the Name property
        {
            //get => _fiscalcode;
            return (2021 - (Year));
        }
        public string nomeCompleto()
        {
            return Name + "  " + Surname + " " + _fiscalcode + " " + Age();
        }
    }
    class Actor : Person
    {
        public string Role { get; set; }
        public string rolePlaying()
        {
            return "the actor's name is " + Name + "  " + Surname + " " + Role + Age();
        }
        enum ActorType
        {
            None,
            Protagonist,
            Coprotagonist,
            VoiceActor
        }
        /* public override string StampaMessaggio()
         {
             //c'è un override
             return "questo è il metodo dell'attore ";
         }*/
    }
}